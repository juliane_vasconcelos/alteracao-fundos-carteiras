package br.com.galgo.alteracao;

import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;

public interface Alterar {

	public void alterar(Teste teste);

}