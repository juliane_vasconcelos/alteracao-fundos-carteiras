package br.com.galgo.alterar.portal;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.galgo.alteracao.Alterar;
import br.com.galgo.testes.recursos_comuns.enumerador.Canal;
import br.com.galgo.testes.recursos_comuns.enumerador.Grupo;
import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.config.MassaDados;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtilsCarteiras;
import br.com.galgo.testes.recursos_comuns.file.entidades.Carteira;
import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaAlteracaoCarteira;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaCamposAnbid;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaContasCarteira;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaDatasCarteira;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaDoctosAnexadosCarteira;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaIdentificacaoCarteira;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaPerfilCarteira;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaPrestadoresServicoCarteira;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesFundos;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;
import br.com.galgo.validacao.ValidarCarteiras;

public class AlterarCarteiraPortal implements Alterar {

	Teste teste;
	private final String PASTA_TESTE = "AlterarCarteiraPortal";

	@Before
	public void setUp() {
		Ambiente ambiente = TesteUtils.configurarTeste(Ambiente.HOMOLOGACAO,
				PASTA_TESTE);
		List<Teste> listaTeste = ArquivoUtils.getListaTeste(ambiente,
				ConstantesTestes.CAMINHO_TESTE_FUNDOS,//
				ConstantesFundos.getAbaTeste(ambiente),//
				ConstantesFundos.LINHA_INICIAL_TESTE_FUNDOS,//
				ConstantesFundos.COLUNA_GRUPO_FUNDOS,//
				ConstantesFundos.COLUNA_SERVICO_FUNDOS,//
				ConstantesFundos.COLUNA_CANAL_FUNDOS,//
				ConstantesFundos.COLUNA_ID_FUNDOS,//
				ConstantesFundos.COLUNA_OPERACAO_FUNDOS,//
				ConstantesFundos.COLUNA_DESCRICAO_FUNDOS,//
				ConstantesFundos.COLUNA_RETESTE_FUNDOS);

		Grupo grupo = Grupo.ANTES;
		Servico servico = null;
		Operacao operacao = Operacao.INCLUSAO_CARTEIRA;
		Canal canal = Canal.PORTAL;

		teste = Teste.fromListaTeste(listaTeste, ambiente, grupo, servico,
				operacao, canal);
	}

	@Test
	public void alterar() throws ErroAplicacao {
		alterar(teste);
	}

	@After
	public void tearDown() {
		TesteUtils.finalizarTeste("alterarCarteira.png");
	}

	public void alterar(Teste teste) {
		Ambiente ambiente = teste.getAmbiente();
		final String caminhoMassaDadosFundos = MassaDados.fromAmbiente(
				ambiente, ConstantesTestes.DESC_MASSA_DADOS_FUNDOS).getPath();

		String login = ArquivoUtils.getLogin(teste, caminhoMassaDadosFundos);
		String senha = ArquivoUtils.getSenha(teste, caminhoMassaDadosFundos);
		Usuario usuario = new Usuario(login, senha, ambiente);

		String aba = ArquivoUtils.getAba(teste, caminhoMassaDadosFundos);

		List<Carteira> listaCarteiras = ArquivoUtilsCarteiras.getCarteiras(
				teste, caminhoMassaDadosFundos, aba);

		final String url = ambiente.getUrl();
		TelaGalgo.abrirBrowser(url);

		for (Carteira carteira : listaCarteiras) {
			alterar(usuario, carteira);
			if (carteira.isValidacaoInclusao()) {
				ValidarCarteiras.validarInclusao(carteira
						.getIdentificacaoCarteira().getDocto(), url);
			}
		}
	}

	private void alterar(Usuario usuario, Carteira carteira) {
		TelaLogin telaLogin = new TelaLogin();
		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaAlteracaoCarteira telaAlteracaoCarteira = (TelaAlteracaoCarteira) telaHome
				.acessarSubMenu(SubMenu.ALTERACAO_CARTEIRAS_ADMINISTRADAS);

		TelaConsultaFundo telaConsultaFundo = telaAlteracaoCarteira
				.clicarBotaoAlterarCarteira();
		telaConsultaFundo.incluirFiltroSigla(carteira
				.getIdentificacaoCarteira().getSigla());

		TelaIdentificacaoCarteira telaIdentificacaoCarteira = new TelaIdentificacaoCarteira();

		TelaPerfilCarteira telaPerfilCarteira = telaIdentificacaoCarteira
				.preencherAba(carteira.getIdentificacaoCarteira(),
						Operacao.ALTERACAO_CARTEIRA);
		TelaDatasCarteira telaDatasCarteira = telaPerfilCarteira
				.preencherAba(carteira.getPerfilCarteira());
		TelaPrestadoresServicoCarteira telaPrestadoresServicoCarteira = telaDatasCarteira
				.preencherAba(carteira.getDataInicioAtividade());
		TelaContasCarteira telaContasCarteira = telaPrestadoresServicoCarteira
				.preencherAba(carteira.getPrestadoresServicoCarteira());
		TelaCamposAnbid telaCamposAnbid = telaContasCarteira
				.preencherAba(carteira.getCodSUSEP());
		TelaDoctosAnexadosCarteira telaDoctosAnexadosCarteira = telaCamposAnbid
				.preencherAba(carteira.isDivulgacaoAnbid());
		telaDoctosAnexadosCarteira.preencherAba(carteira
				.getDoctosAnexadosCarteira());
		telaAlteracaoCarteira.validaAlteracaoSucesso();
		telaLogin.logout();
	}
}
