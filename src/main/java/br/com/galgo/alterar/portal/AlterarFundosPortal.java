package br.com.galgo.alterar.portal;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.galgo.alteracao.Alterar;
import br.com.galgo.testes.recursos_comuns.enumerador.Canal;
import br.com.galgo.testes.recursos_comuns.enumerador.Grupo;
import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.config.MassaDados;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtilsFundos;
import br.com.galgo.testes.recursos_comuns.file.entidades.Fundo;
import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaAlteracaoFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaAmortizacao;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaContasFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaDatasFundos;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaDoctosAnexadosFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaEventos;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaIdentificacaoFundoAlteracao;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaParametrosMovimentacao;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaPerfilFundosAlteracao;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaPrestadoresServicoFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaTaxasFundos;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesFundos;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;
import br.com.galgo.validacao.ValidarFundos;

public class AlterarFundosPortal implements Alterar {

	Teste teste;
	Operacao operacao = Operacao.ALTERACAO_FUNDO;
	private final String PASTA_TESTE = "AlterarFundoPortal";

	@Before
	public void setUp() throws Exception {
		Ambiente ambiente = TesteUtils.configurarTeste(Ambiente.HOMOLOGACAO,
				PASTA_TESTE);
		List<Teste> listaTeste = ArquivoUtils.getListaTeste(ambiente,
				ConstantesTestes.CAMINHO_TESTE_FUNDOS,//
				ConstantesFundos.getAbaTeste(ambiente),//
				ConstantesFundos.LINHA_INICIAL_TESTE_FUNDOS,//
				ConstantesFundos.COLUNA_GRUPO_FUNDOS,//
				ConstantesFundos.COLUNA_SERVICO_FUNDOS,//
				ConstantesFundos.COLUNA_CANAL_FUNDOS,//
				ConstantesFundos.COLUNA_ID_FUNDOS,//
				ConstantesFundos.COLUNA_OPERACAO_FUNDOS,//
				ConstantesFundos.COLUNA_DESCRICAO_FUNDOS,//
				ConstantesFundos.COLUNA_RETESTE_FUNDOS);
		Grupo grupo = Grupo.ANTES;
		Servico servico = null;
		Canal canal = Canal.PORTAL;

		teste = Teste.fromListaTeste(listaTeste, ambiente, grupo, servico,
				operacao, canal);
	}

	@Test
	public void alterar() throws ErroAplicacao {
		alterar(teste);
	}

	@After
	public void tearDown() {
		TesteUtils.finalizarTeste("alterarFundos.png");
	}

	public void alterar(Teste teste) {
		Ambiente ambiente = teste.getAmbiente();
		final String caminhoMassaDadosFundos = MassaDados.fromAmbiente(
				ambiente, ConstantesTestes.DESC_MASSA_DADOS_FUNDOS).getPath();

		String login = ArquivoUtils.getLogin(teste, caminhoMassaDadosFundos);
		String senha = ArquivoUtils.getSenha(teste, caminhoMassaDadosFundos);
		Usuario usuario = new Usuario(login, senha, ambiente);

		String aba = ArquivoUtils.getAba(teste, caminhoMassaDadosFundos);

		List<Fundo> listaFundos = ArquivoUtilsFundos.getFundos(teste,
				caminhoMassaDadosFundos, aba, operacao);

		final String url = ambiente.getUrl();
		TelaGalgo.abrirBrowser(url);

		for (Fundo fundo : listaFundos) {
			alterar(usuario, fundo);
			ValidarFundos.validarAlteracao(fundo.getIdentificaoFundo()
					.getCnpj(), url, ambiente);
		}
	}

	private void alterar(Usuario usuario, Fundo fundo) {
		TelaLogin telaLogin = new TelaLogin();
		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaAlteracaoFundo telaAlteracaoFundo = (TelaAlteracaoFundo) telaHome
				.acessarSubMenu(SubMenu.ALTERACAO_FUNDOS_INVESTIMENTO);

		TelaConsultaFundo telaConsultaFundo = telaAlteracaoFundo
				.clicarBotaoAlterarFundo();

		telaConsultaFundo.incluirFiltroCNPJ(fundo.getIdentificaoFundo()
				.getCnpj());

		TelaIdentificacaoFundoAlteracao telaIdentificacaoFundo = new TelaIdentificacaoFundoAlteracao();
		TelaEventos telaEventos = telaIdentificacaoFundo.preencherAba(fundo
				.getIdentificaoFundo());
		TelaPerfilFundosAlteracao telaPerfil = telaEventos.preencherAba(
				fundo.getEvento(), operacao);
		TelaDatasFundos telaDatasFundos = telaPerfil.preencherAba(fundo
				.getPerfil());
		TelaPrestadoresServicoFundo telaPrestadoresServico = telaDatasFundos
				.preencherAba(fundo.getDatasFundos());
		TelaTaxasFundos telaTaxas = telaPrestadoresServico.preencherAba(fundo
				.getPrestadoresServico());
		TelaParametrosMovimentacao telaParametrosMovimentacao = telaTaxas
				.preencherAba(fundo.getTaxasFundos());
		TelaAmortizacao telaAmortizacao = telaParametrosMovimentacao
				.preencherAba(fundo.getParametrosMovimentacao());
		TelaContasFundo telaContas = telaAmortizacao.preencherAba(fundo
				.getAmortizacao());
		TelaDoctosAnexadosFundo telaDocumentosAnexados = telaContas
				.preencherAba(fundo.getContas());
		telaDocumentosAnexados
				.preencherAba(fundo.getDoctosAnexados(), operacao);
		telaLogin.logout();
	}
}
