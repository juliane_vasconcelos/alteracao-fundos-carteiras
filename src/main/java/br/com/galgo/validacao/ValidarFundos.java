package br.com.galgo.validacao;

import org.junit.Assert;

import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaValidacaoAlteracaoFundos;

public class ValidarFundos {

	public static void validarAlteracao(String cnpj, String url,
			Ambiente ambiente) {

		TelaLogin telaLogin = new TelaLogin();
		Usuario usuario = getUsuario(ambiente);

		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaValidacaoAlteracaoFundos telaValidacaoAlteracaoFundos = (TelaValidacaoAlteracaoFundos) telaHome
				.acessarSubMenu(SubMenu.VALIDACAO_ALTERACAO_FUNDOS);
		telaValidacaoAlteracaoFundos
				.clicarValidacaoInclusaoRegistro(telaValidacaoAlteracaoFundos);
		telaValidacaoAlteracaoFundos.verificaUltimaPagina();
		Assert.assertTrue("Fundo não incluído com sucesso",
				telaValidacaoAlteracaoFundos.verificaTextoNaTela(cnpj));
	}

	private static Usuario getUsuario(Ambiente ambiente) {
		Usuario usuario = null;
		if (Ambiente.HOMOLOGACAO == ambiente) {
			usuario = new Usuario(UsuarioConfig.AUTOREG_HOMOLOG);
		} else {
			usuario = new Usuario(UsuarioConfig.ANBIMA);
		}
		return usuario;
	}
}
